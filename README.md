# C++11 lock-free work-stealing deque

[![pipeline status](https://gitlab.com/m1ke.v4n.dyke/chase_lev_deque/badges/master/pipeline.svg)](https://gitlab.com/m1ke.v4n.dyke/chase_lev_deque/commits/master)

C++11 implementation of the **Chase-Lev work-stealing deque**, which is a lock-free single-producer, multi-consumer double ended queue.

Push operations are always successful.
Pop operations may fail if deque is empty or due to concurrency on one of the deque ends (or on both if the current size of the deque is 1).

This repository contains two different Chase-Lev deques.
1. [Dynamic Circular Work-Stealing Deque](https://www.dre.vanderbilt.edu/~schmidt/PDF/work-stealing-dequeue.pdf) by David Chase and Yossi Lev.
This deque can be found in the `namespace cld` of `"lockless_deque.h"`.
2. [Correct and Efficient Work-Stealing for Weak Memory Models](http://www.di.ens.fr/~zappa/readings/ppopp13.pdf) by Nhat Minh Le et al..
This deque can be found in the `namespace ocld` of `"lockless_deque.h"`.

## API

```cpp
namespace cld {
template <typename T>
class lockless_deque {
  public:
    lockless_deque();

    /**
     * Pushes `item` to the front of the deque
     * 
     */
    void push_front(const T& item);

    /**
     * Pops an element from the front of the deque and assigns it to `item`
     * 
     * returns bool - if pop operation was successful
     * returns false - otherwise
     * 
     */
    bool pop_front(T& item);

    /**
     * Pops an element from the back of the deque and assigns it to `item`
     * 
     * returns bool - if pop operation was successful
     * returns false - otherwise
     * 
     */
    bool pop_back(T& item);
} // class lockless_deque
} // namespace cld
```

## Usage

```cpp
#include "lockless_deque.h"

cld::lockless_deque<int> deque;

/* Producer may use push_front and pop_front */
deque.push_front(3);
int item = 0;
if(deque.pop_front(item)) {
    // process item
}

/* Consumer may only use pop_back */
int item = 0;
if(deque.pop_back(item)) {
    // process item
}

```