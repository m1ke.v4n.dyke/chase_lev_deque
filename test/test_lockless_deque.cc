/**
 *
 * MIT License
 *
 * Copyright (c) 2018 Michael Schmid
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <thread>
#include <vector>
#include "circular_array.h"
#include "lockless_deque.h"

#include <atomic>

template <typename D>
void producer(D& q, std::vector<int>& vec) {
  for (int i = 0; i < 10000; ++i) {
    q.push_front(i);
  }

  int elem = 0;
  for (int i = 0; i < 1000; ++i) {
    while (!q.try_pop_back(elem)) {
    }
    vec.at(elem) = elem;
  }
}

template <typename D>
void consumer(D& q, std::vector<int>& vec) {
  int elem = 5555;
  for (int i = 0; i < 1000; ++i) {
    while (!q.try_pop_back(elem)) {
    }
    vec.at(elem) = elem;
  }
}

template <typename T>
class TestLocklessDeque : public ::testing::Test {
 public:
  T deque;
};

typedef ::testing::Types<
    cld::lockless_deque<int> , ocld::lockless_deque<int>>
    deque_types;
TYPED_TEST_CASE(TestLocklessDeque, deque_types);

TEST(TestLocklessDeque, TestCircularArray) {
  utils::circular_array<int>* ca = new utils::circular_array<int>{8};

  ca->put(2, 22);
  ca->put(3, 33);
  ca = ca->grow(32, 0);
  ca->put(5, 55);
  ca->put(6, 66);

  EXPECT_EQ(22, ca->get(2));
  EXPECT_EQ(33, ca->get(3));
  EXPECT_EQ(55, ca->get(5));
  EXPECT_EQ(66, ca->get(6));

  delete ca;
}

TYPED_TEST(TestLocklessDeque, TestDequeFunctions) {
  int value = 0;
  bool first_pop_front = this->deque.try_pop_front(value);
  bool first_pop_back = this->deque.try_pop_back(value);

  this->deque.push_front(value + 10);
  this->deque.push_front(value + 20);
  this->deque.push_front(value + 30);
  this->deque.push_front(value + 40);

  EXPECT_FALSE(first_pop_front);
  EXPECT_FALSE(first_pop_back);
  EXPECT_TRUE(this->deque.try_pop_back(value));
  EXPECT_EQ(10, value);
  EXPECT_TRUE(this->deque.try_pop_front(value));
  EXPECT_EQ(40, value);
  EXPECT_TRUE(this->deque.try_pop_back(value));
  EXPECT_EQ(20, value);
  EXPECT_TRUE(this->deque.try_pop_front(value));
  EXPECT_EQ(30, value);
}

TYPED_TEST(TestLocklessDeque, TestConcurrency) {
  std::vector<int> vec(10000, 0);
  std::vector<int> res_vec;
  std::vector<std::thread> cons;

  for (int i = 0; i < 10000; ++i) {
    res_vec.push_back(i);
  }

  std::thread prod{producer<decltype(this->deque)>, std::ref(this->deque),
                   std::ref(vec)};

  for (int i = 0; i < 9; ++i) {
    cons.emplace_back(consumer<decltype(this->deque)>, std::ref(this->deque),
                      std::ref(vec));
  }
  prod.join();

  for (auto& t : cons) {
    t.join();
  }
  EXPECT_TRUE(std::equal(vec.begin(), vec.end(), res_vec.begin()));
}
