/**
 *
 * MIT License
 *
 * Copyright (c) 2018 Michael Schmid
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
#ifndef UTILS_CIRCULAR_ARRAY_H_
#define UTILS_CIRCULAR_ARRAY_H_

namespace utils {

template <typename T>
class circular_array {
  typedef T value_type;

 private:
  std::unique_ptr<circular_array<T>> garbage_collector;
  std::size_t log_size;
  T* segments;

 public:
  explicit circular_array(std::size_t s) : log_size(s) {
    segments = new T[1 << log_size];
  }
  circular_array(circular_array&&) = delete;
  circular_array(const circular_array&) = delete;
  circular_array& operator=(circular_array&&) = delete;
  circular_array& operator=(const circular_array&) = delete;
  ~circular_array() {
    if (segments) {
      delete[] segments;
    }
  }

  long size() const { return 1 << log_size; }

  T get(std::size_t index) { return segments[index % size()]; }

  void put(std::size_t index, const T& element) {
    segments[index % size()] = element;
  }

  circular_array<T>* grow(std::size_t f, std::size_t b) {
    circular_array<T>* next_array = new circular_array<T>(log_size + 1);
    next_array->garbage_collector.reset(this);
    for (std::size_t i = b; i < f; ++i) {
      next_array->put(i, get(i));
    }

    return next_array;
  }
};
}  // namespace utils

#endif /* UTILS_CIRCULAR_ARRAY_H_ */