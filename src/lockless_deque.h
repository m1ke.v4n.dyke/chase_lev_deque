/**
 *
 * MIT License
 *
 * Copyright (c) 2018 Michael Schmid
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
#ifndef CLD_LOCKLESS_DEQUE_H_
#define CLD_LOCKLESS_DEQUE_H_

#include <atomic>
#include <memory>
#include "circular_array.h"

namespace cld {

using utils::circular_array;
template <typename T>
class lockless_deque {
 private:
  std::atomic<long> front;
  std::atomic<long> back;
  std::atomic<circular_array<T>*> elements;

 public:
  lockless_deque() : front(0), back(0), elements(new circular_array<T>(16)) {}
  lockless_deque(lockless_deque&&) = delete;
  lockless_deque(const lockless_deque&) = delete;
  lockless_deque& operator=(lockless_deque&&) = delete;
  lockless_deque& operator=(const lockless_deque&) = delete;
  ~lockless_deque() {
    circular_array<T>* a = elements.load();
    if (a)
      delete a;
  }

  void push_front(const T& element) {
    long f = front.load();
    long b = back.load();

    circular_array<T>* a = elements.load();

    long size = f - b;
    if (size >= static_cast<long>(a->size()) - 1) {
      a = a->grow(f, b);
      elements.store(a);
    }
    a->put(f, element);
    front.store(f + 1);
  }

  bool try_pop_front(T& element) {
    long f = front.load();
    circular_array<T>* a = elements.load();

    f = f - 1;

    front.store(f);

    long b = back.load();
    long size = f - b;

    if (size < 0) {
      front.store(b);
      return false;
    }

    element = a->get(f);

    if (size > 0) {
      return true;
    }
    bool successful_cas = true;
    long tmp = b;
    if (!back.compare_exchange_strong(tmp, b + 1)) {
      successful_cas = false;
    }

    front.store(b + 1);
    return successful_cas;
  }

  bool try_pop_back(T& element) {
    long b = back.load();
    long f = front.load();

    circular_array<T>* a = elements.load();

    long size = f - b;
    if (size <= 0) {
      return false;
    }

    element = a->get(b);
    if (!back.compare_exchange_strong(b, b + 1)) {
      return false;
    }

    return true;
  }
};

}  // namespace cld

namespace ocld {
using utils::circular_array;

template <typename T>
class lockless_deque {
 private:
  std::atomic<long> front;
  std::atomic<long> back;
  std::atomic<circular_array<T>*> elements;

 public:
  lockless_deque() : front(0), back(0), elements(new circular_array<T>(32)) {}
  lockless_deque(lockless_deque&&) = delete;
  lockless_deque(const lockless_deque&) = delete;
  lockless_deque& operator=(lockless_deque&&) = delete;
  lockless_deque& operator=(const lockless_deque&) = delete;
  ~lockless_deque() {
    circular_array<T>* a = elements.load();
    if (a)
      delete a;
  }

  void push_front(const T& item) {
    long f = front.load(std::memory_order_relaxed);
    long b = back.load(std::memory_order_acquire);
    circular_array<T>* a = elements.load(std::memory_order_relaxed);

    if ((f - b) > (a->size() - 1)) {
      a = a->grow(f, b);
      elements.store(a);
    }
    a->put(f, item);
    std::atomic_thread_fence(std::memory_order_release);
    front.store(f + 1, std::memory_order_relaxed);
  }

  bool try_pop_front(T& item) {
    long f = front.load(std::memory_order_relaxed) - 1;
    circular_array<T>* a = elements.load(std::memory_order_relaxed);
    front.store(f, std::memory_order_relaxed);
    std::atomic_thread_fence(std::memory_order_seq_cst);

    long b = back.load(std::memory_order_relaxed);

    bool successful = true;
    if (b <= f) {
      item = a->get(f);
      if (f == b) {
        if (!back.compare_exchange_strong(b, b + 1, std::memory_order_seq_cst,
                                          std::memory_order_relaxed)) {
          successful = false;
        }
        front.store(f + 1, std::memory_order_relaxed);
      }
    } else {
      successful = false;
      front.store(f + 1, std::memory_order_relaxed);
    }

    return successful;
  }

  bool try_pop_back(T& item) {
    long b = back.load(std::memory_order_acquire);
    std::atomic_thread_fence(std::memory_order_seq_cst);
    long f = front.load(std::memory_order_acquire);

    bool successful = false;
    if (b < f) {
      circular_array<T>* a = elements.load(std::memory_order_consume);
      item = a->get(b);
      if (!back.compare_exchange_strong(b, b + 1, std::memory_order_seq_cst,
                                        std::memory_order_relaxed)) {
        return false;
      }
      successful = true;
    }
    return successful;
  }
};

}  // namespace ocld

#endif /* CLD_LOCKLESS_DEQUE_H_ */
